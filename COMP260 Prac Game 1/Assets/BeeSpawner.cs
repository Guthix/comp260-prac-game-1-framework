﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {

	public BeeMove beePrefab;
	public PlayerMove player;
	//public Transform player2;

	void Start() {
		// instantiate the bees
		BeeMove bee = Instantiate(beePrefab);
		// set the target 
		bee.target = player;
		//bee.target2 = player2;
	}

	
	// Update is called once per frame
	void Update () {
		
	}
}
