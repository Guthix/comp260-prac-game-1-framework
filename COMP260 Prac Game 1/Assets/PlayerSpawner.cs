﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour {

	public PlayerMove playerPrefab;

	void Start () {
		// instantiate a player
		PlayerMove player = Instantiate(playerPrefab);
	}

	
	// Update is called once per frame
	void Update () {
		
	}
}
