﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	public Vector2 move;
	public Vector2 velocity; // in metres per second
	public float maxSpeed = 5.0f; // in metres per second
	public float acceleration = 5.0f; // in metres/second/second
	private float speed = 0.0f;    // in metres/second
	public float brake = 5.0f; // in metres/second/second
	public float turnSpeed = 30.0f; // in degrees/second


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update() {

		// the horizontal axis controls the turn
		float turn = Input.GetAxis("Horizontal");

		// turn the car
		transform.Rotate(0, 0, turn * turnSpeed * Time.deltaTime * speed);



		// the vertical axis controls acceleration fwd/back
		float forwards = Input.GetAxis("Vertical");
		if (forwards > 0) {
			// accelerate forwards
			speed = speed + acceleration * Time.deltaTime;
		}
		else if (forwards < 0) {
			// accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
		} else {
			// braking
			if (speed > 0) { //car is moving forward
				speed = Mathf.Max(0, speed - brake * Time.deltaTime);
			} else { //car is moving backward
				speed = Mathf.Min(0, speed + brake * Time.deltaTime);
			}
		}



		// clamp the speed
		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);


		// compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;

		// move the object
		transform.Translate(velocity * Time.deltaTime, Space.Self);


/*		// get the input values
		Vector2 direction;


//		direction.x = Input.GetAxis ("Horizontal");
//		direction.y = Input.GetAxis ("Vertical");

		if (gameObject.name == "Player1") {

			direction.x = Input.GetAxis ("Horizontal");
			direction.y = Input.GetAxis ("Vertical");


		} else { //the object is named something that's not PLayer1, Player2 in this case
			direction.x = Input.GetAxis ("Horizontal2");
			direction.y = Input.GetAxis ("Vertical2");


		}



		// scale by the maxSpeed parameter


		Vector2 velocity = direction * maxSpeed;

		// move the object
		transform.Translate (velocity * Time.deltaTime);
		*/
	}
}
