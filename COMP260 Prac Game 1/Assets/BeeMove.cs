﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {


	/* NOTE: This is for the bee following to players, for 1 player it's down the bottom
	public float speed = 4.0f;        // metres per second
	public float turnSpeed = 180.0f;  // degrees per second
	public Transform target;
	public Transform target2;
	private Vector2 heading = Vector2.right; 



	// Use this for initialization
	void Start() {
		// find a player object to be the target by type
		PlayerMove player = 
			(PlayerMove) FindObjectOfType(typeof(PlayerMove));
		target = player.transform;
	}

	
	// Update is called once per frame
	void Update () {

		// get the vector from the bee to the target 

		Vector2 distanceFrom1 = target.position - transform.position;
		Vector2 distanceFrom2 = target2.position - transform.position;


		Vector2 direction;

		if (distanceFrom1.magnitude < distanceFrom2.magnitude) { //Player1 is closer
			direction = target.position - transform.position;
		} else {//Player2 is closer
			direction = target2.position - transform.position;
		}



		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		// turn left or right
		if (direction.IsOnLeft(heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		}
		else {
			// target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}

		transform.Translate(heading * speed * Time.deltaTime);
	}

	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;

		Vector2 distanceFrom1 = target.position - transform.position;
		Vector2 distanceFrom2 = target2.position - transform.position;

		Vector2 direction;

		if (distanceFrom1.magnitude < distanceFrom2.magnitude) { //Player1 is closer
			direction = target.position - transform.position;
		} else {//player2 is closer
			direction = target2.position - transform.position;
		}


		Gizmos.DrawRay(transform.position, direction);
	}
	*/

	public float speed = 4.0f;        // metres per second
	public float turnSpeed = 180.0f;  // degrees per second
	public Transform target;
	private Vector2 heading = Vector2.right; 

	void Update() {
		// get the vector from the bee to the target 
		Vector2 direction = target.position - transform.position;

		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		// turn left or right
		if (direction.IsOnLeft(heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		}
		else {
			// target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}

		transform.Translate(heading * speed * Time.deltaTime);
	}


	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Gizmos.DrawRay(transform.position, direction);
	}


}
